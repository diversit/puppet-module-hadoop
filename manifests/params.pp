# /etc/puppet/modules/hadoop/manifests/params.pp

class hadoop::params {

	include java::params

	$version = $::hostname ? {
		default			=> "1.0.2",
	}
        
	$master = $::hostname ? {
		default			=> "hadoopmaster.hadoop.local",
	}
        
	$slaves = $::hostname ? {
		default			=> ["hadoop-01.hadoop.local", "hadoop-02.hadoop.local", "hadoop-03.hadoop.local", "hadoop-04.hadoop.local"] 
	}
	$hdfsport = $::hostname ? {
		default			=> "8020",
	}

	$replication = $::hostname ? {
		default			=> "3",
	}

	$jobtrackerport = $::hostname ? {
		default			=> "8021",
	}
	$java_home = $::hostname ? {
		default			=> "${java::params::java_base}/jdk${java::params::java_version}",
	}
	$hadoop_base = $::hostname ? {
		default			=> "/opt/hadoop",
	}
	$hdfs_path = $::hostname ? {
		default			=> "/home/hduser/hdfs",
	}
}
